const express = require("express");
const bodyParser = require("body-parser");
const router = express.Router();
const app = express();
const fs = require("fs");
const path = require("path");

const directoryPath = path.join(__dirname, 'files');

app.use(bodyParser.urlencoded({extended: false}));
app.use(bodyParser.json());

const extArr = ["log", "txt", "json", "yaml", "xml", "js"];

//POST method
router.post('/api/files', (request, response) => {
    if (!fs.existsSync(directoryPath)) {
        fs.mkdirSync(directoryPath);
    }

    if (request.body.content === undefined) {
        fs.appendFile('logs.log', 'Code 400: Please specify \'content\' parameter.\n', () => {});
        return response.status(400).json({message: "Please specify 'content' parameter"});
    } else if (request.body.filename === undefined) {
        fs.appendFile('logs.log', 'Code 400: Please specify \'content\' parameter.\n', () => {
        });
        return response.status(400).json({message: "Please specify 'filename' parameter"});
    }
    fs.writeFile(`files/${request.body.filename}`, `${request.body.content.toString()}`, function (err) {
        if (err) {
            return response.status(400).json({message: "Client error"});
        } else {
            fs.appendFile('logs.log', 'Code 200: File is created successfully.\n', () => {});
            return response.status(200).json({message: "File is created successfully."});
        }
    });
});

//GET for all files
router.get('/api/files', (request, response) => {
    if (!fs.existsSync(directoryPath)) {
        fs.mkdirSync(directoryPath);
    }

    fs.readdir(directoryPath, (err, files) => {
        if (err) {
            fs.appendFile('logs.log', 'Code 400: Client error.\n', () => {
            });
            return response.status(400).json({message: "Client error"});
        }
        let res = [];
        files.forEach(file => {
            if (!extArr.includes(file.split('.').slice(1).join('.'))){
                //pass
            }
            res.push(file);
        });
        fs.appendFile('logs.log', 'Code 200: All files have successfully been read.\n', () => {});
        return response.status(200).json({
            message: "All files have successfully been read",
            files: res
        });
    });
})


//GET by filename
router.get('/api/files/:name', (request, response) => {
    if (!fs.existsSync(directoryPath)) {
        fs.mkdirSync(directoryPath);
    }

    let filename = request.params.name;

    fs.stat(`./files/${filename}`, (error, stats) => {
        fs.readFile(`files/${filename}`, 'utf8', (err, data) => {
            if (err) {
                fs.appendFile('logs.log', `Code 400: No file with '${filename}' filename found.\n`, () => {
                });
                return response.status(400).json({message: `No file with '${filename}' filename found`});
            }
            let res = {};
            res[filename] = data;
            fs.appendFile('logs.log', 'Code 200: File received successfully.\n', () => {});

            if (!extArr.includes(filename.split('.').pop())){
                return response.status(400).json({message: "Wrong extension"});
            }
            return response.status(200).json({
                message: "File received successfully",
                filename: filename,
                content: data,
                extension: filename.split('.').pop(),
                uploadedDate: stats.birthtime.toUTCString()
            });
        });
    });
});

app.use("/", router);

app.listen(8080, function () {
    console.log('Example app listening on port 8080!');
});
